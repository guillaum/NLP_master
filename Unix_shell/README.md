## Shell features

  * command conventions
    * `man ls`
    * `man grep`
  * glob patterns
  * input/output/append to file (`>`, `>>` and `2>`)
  * pipes
  * variables
  * `$PATH` (`./script` vs `script`)
  * tab completion
  * permissions: `ls -l`

## Commands for everyday

  * `man`
  * `pwd`
  * `cd`
  * `ls`
  * `cp`
  * `mv`
  * `rm`
  * `mkdir`
  * `grep`

## Other commands

  * `echo`
  * `cat`
  * `less`
  * `rmdir`
  * `touch`
  * `chmod`
  * `chown`
  * `wc`
  * `basename`
  * `sort`
  * `head`
  * `tr`
  * `sed`
  * `diff`
  * `find`
  * `cut`
  * `xargs`
  * `ps`
  * `sudo`
  * `su`
  * `tar`
  * `ln`
  * `nano`
  * `ssh`
  * `scp`

## Scripting

  * Many ≠ shell languages! (see [StackOverflow](https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash))
  * shebang
  * arguments
  * comments
  * $?, test, expr, back-quotes
  * read
  * control flow (if, case, for, while, until) -> see [Shell Programming](http://www-h.eng.cam.ac.uk/help/tpl/unix/scripts/node2.html)

## Exercises

Run exercises [here](http://www-h.eng.cam.ac.uk/help/tpl/unix/scripts/node16.html)

Input for exercise 1:

```sh
#!/bin/sh
echo This $0 command has $# arguments.
if [ $# != 0 ]
then
  echo There are $*
fi
```


