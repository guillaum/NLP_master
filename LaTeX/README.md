# Introduction to LaTeX

## Online courses
Two available online courses from University of Bristol (Dr John D. Lees-Miller):

 * [Part 1](https://www.overleaf.com/latex/learn/free-online-introduction-to-latex-part-1.pdf)
 * [Part 2](https://www.overleaf.com/latex/learn/free-online-introduction-to-latex-part-2.pdf)

## How to use LaTeX
 * Use your favorite texteditor and compile on your labtop: `pdflatex`
 * Online LaTeX: [Overleaf](https://www.overleaf.com). Select create a new paper
 * More options: [8 Best LaTeX Editors](https://beebom.com/best-latex-editors/)

## Practice
Download [LaTeX_exercices.pdf](LaTeX_exercices.pdf).

Download the starting point: [main.tex](main.tex)

If you never used LaTeX before:

 * Read the course above (Part 1)
 * Run exercice 1
 * Read the course above (Part 2)
 * Run exercice 2

If you have already used LaTex:

 * Run exercice 1
 * Run exercice 2
 * Run exercice 3

## Some packages

 * `\usepackage{amsmath}`
 * `\usepackage{graphicx}`
 * `\usepackage[utf8]{inputenc}`
 * `\usepackage[T1]{fontenc}`
 * `\usepackage[french]{babel}`
 * A huge number of other packages…

## Resources

 * [LaTeX for linguistics](https://en.wikibooks.org/wiki/LaTeX/Linguistics)
 * [Detexify](http://detexify.kirelabs.org/classify.html)
 * [Tikz examples](http://www.texample.net/tikz/examples/)
 * Search the web for: "LaTeX …"
